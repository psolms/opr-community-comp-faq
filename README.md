# OPR Competitive Rules FAQ

This document has been written to help differentiate the as-written rules of OPR from common house-rules and misconceptions.
All interpretations are for OPR v 2.50

# General Rules
## Cover
- When does a unit get or lose cover?
    - A unit recieves cover when standing in or behind a piece of cover terrain
    - If a unit can draw 2D LOS to **ANY** model base in the unit without interesecting cover terrain the unit does not have cover.

- Does LOS contribute to cover?
    - No LOS does not contribute or provide cover it only removes

- Do enemy and ally units provide cover?
    - No only terrain

## Blast
- When do you determine Blast?
    - At decleration you count the amount of models in the targetted unit. If they are greater or equal to the blast number you get that many extra hits per a successful hits.
    - If the targetted unit has less models then your blast number at decleration you get that model counts extra hits instead.

- Can Blast wound rolls be reduced by units model count?
    - No blast only affects the amount of hits that leads to wound rolls

## Transport
- Where do you measure for models leaving a transport?
    - You place models base to base with the transport and then measure 6" move as normal.

## Elevation
- How does elevation affect LOS?
    - All LOS is 2D in OPR, elevation has no effect.

- Elevation and movement
    - Elevation terrain has a movement penalty associated with (ie -3" or -6" to move)

## Poison

## Aircraft
- How do aircraft interact with terrain and units on the board?
    - Aircraft do not interact with the board for movement. If the aircraft cannot move onto a spot due to terrain or units you can use a token to represent it. Measurements for future movement still must use the base as normal.

- How does LOS work on aircraft?
    - LOS on aircraft works as normal and must respect 2D, Terrain, and Units.

# Faction Specific Rules
## Tao
- Are targetting lasers affected by attack penalties?
    - No targetting lasers are always a 4+ roll

- Is there any difference between Targetting Lasers and Drones (Targetting Lasers)?
    - No except for their expressed limitations in list building


To be edited and added

"
- No timing defined: when can it be used? At any point? only during the shooting?
- instead of firing one of its weapons: what is "firing a weapon"? A weapon that actually has a target and you would roll attacks for it or does it just not allow to use that weapon, no matter if target available or not?
- again the missing timing makes the applied markers usable by their own unit or not, depending on what the exact timing for the special rule is.

A completely legit way to interpret it: Use Spotting Laser whenever and cross out a ranged weapon for the shooting later in the activation. Markers are usable by the same unit that applied them.

An also completely legit but completely different interpretation: You can only exchange a weapon that actually rolls attacks during a shooting to shoot the Spotting Laser, which makes the Markers not usable for the unit that applies them, because all shooting happens simultaneously and does not influence each other, so the Spotting Laser shot during the shooting would not be able to influence the other weapons during the same shooting.
" - Bara
